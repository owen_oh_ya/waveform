1 NN model compilation
  note:nvidia said it only supports nv small with int8 for vp now.
  note:kmd and umd compilation refer to Filip's readme.
1)	Add below code to support int8 print result. Previous print code from Filip is for fp16 mode in RuntimeTest.cpp.

    //andy add for bpe=1;pftype=1;
    NvS8* pSrc_int8 = reinterpret_cast<NvS8*>(pOutput->m_pData);
    NvS8 maxval_int8 = -128;
    NvU32 maxindex_int8 = -1;

    NvS8 bpe=pOutput->getBpe();
    NvDlaImage::PixelFormatType pftype = pOutput->getPixelFormatType();
    NvDlaDebugPrintf("andy bpe=%d;pftype=%d\n",bpe,pftype);

    for (NvU32 ii=0; ii < pOutput->m_meta.channel; ii++)
    {
            NvDlaDebugPrintf("int8 result[%d]=%u\n", ii, NvS8(pSrc_int8[ii]));

        if (NvS8(pSrc_int8[ii]) > maxval_int8)
        {
        maxval_int8 = NvS8(pSrc_int8[ii]);
                maxindex_int8 = ii;
        }
    }
    // also print using exponential notation (to compare w/ host simulation)
    //for (NvU32 ii=0; ii < pOutput->m_meta.channel; ii++)
    //{
    //        NvDlaDebugPrintf("result[%d]=%e\n", ii, float(pSrc[ii]));
    //}

    NvDlaDebugPrintf("Recognized float digit: %d\n", maxindex);
NvDlaDebugPrintf("Recognized int8 digit: %d\n", maxindex_int8);
2)	Use below fast-math profile for NN model compilation.
  note: basic profile has problem.
   ./nvdla_compiler --prototxt ResNet-50-deploy.prototxt --caffemodel ResNet-50-model.caffemodel --configtarget nv_small --profile fast-math --cprecision int8 --calibtable /remote/ailab2/zhaoqing/nvdla/sw-new/umd/utils/calibdata/resnet50.json


2 v2 cmodel compilation

1)	Define nv_small in tree.make, then execute  tmake -build vmod, so that we can get expected configuration file in outdir.
2)	Copy below configuration file to cmodel/include
    Copy outdir/nv_small/spec/defs/project.h
    Copy outdir/nv_small/spec/manual/ opendla.h 
    Copy outdir/nv_small/spec/manual/ opendla.uh 
3)	Add below file into nvdla tlmcreator compilation setting.  Then you can compile nvdla cmodel in tlmcreator.
    cmod/ nvdla_clibs/ NvdlaPacker.cpp

3 vdk compilation and run
  renamed nvdla_small from nvdla_os_initial in below file.

------------------------------------
'nvdla.dts.txt' in 'workspace/testVDK/vpconfigs/Mini_AArch64_Linux/device_tree_input' with the
   following content:

extmem@70000000 {
        reg = <0x0 0x70000000 0x0 0x10000000>;
        compatible = "nvidia,nvdla-extmem";
};

nvdla@0x43000000 {
        interrupts = <0x0 0x2 0x4>;
        reg = <0x0 0x43000000 0x0 0x20000>;
        compatible = "nvidia, nv_small";
};

example for runtime command:
../nvdla_runtime --loadable ResNet-50-OL.nvdla --image violin.jpg
